<?php

namespace App\Controller;

use App\Entity\GameClaim;
use App\Repository\GameRepository;
use App\Repository\StudioRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AcceptClaimGameController extends AbstractController
{
    public function __invoke($data, ManagerRegistry $doctrine,
             GameRepository $gameRepository, StudioRepository $studioRepository) {

        $entityManager = $doctrine->getManager();
        $game = $gameRepository->find($data->getGame()->getId());
        if (is_null($game->getStudio())) {
            $studio = $studioRepository->find($data->getClaimer()->getId());
            $game->setStudio($studio);
            $entityManager->persist($game);
        }
        $entityManager->remove($data);
        $entityManager->flush();
    }
}