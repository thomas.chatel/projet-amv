<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class TokenInfosController extends AbstractController {

    public function __invoke() {
        $user = new \stdClass;
        $user->id = $this->getUser()->getId();
        $user->roles = $this->getUser()->getRoles();
        return $user;
    }
}