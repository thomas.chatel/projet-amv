<?php

namespace App\Controller;

use App\Repository\StudioRepository;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AcceptClaimStudioController extends AbstractController
{
    public function __invoke($data, ManagerRegistry $doctrine,
             UserRepository $userRepository, StudioRepository $studioRepository) {

        $entityManager = $doctrine->getManager();
        $studio = $studioRepository->find($data->getStudio()->getId());
        if (is_null($studio->getUser())) {
            $user = $userRepository->find($data->getClaimer()->getId());
            $studio->setUser($user);
            $entityManager->persist($studio);
            $roles = $user->getRoles();
            array_push($roles, "ROLE_STUDIO");
            $user->setRoles($roles);
            $entityManager->persist($user);
        }
        $entityManager->remove($data);
        $entityManager->flush();
    }
}