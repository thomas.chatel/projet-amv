<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AddAdminController extends AbstractController
{

    public function __invoke(User $data, ManagerRegistry $doctrine) {

        $entityManager = $doctrine->getManager();

        $roles = $data->getRoles();
        array_push($roles, 'ROLE_ADMIN');
        $data->setRoles($roles);
        $entityManager->persist($data);

        return $data;
    }
}