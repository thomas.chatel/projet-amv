<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetGameController extends AbstractController {

    public function __invoke($data, HttpClientInterface $client, Request $request) {
        // get likes
        $response = $client->request('GET', "http://likes-api:3000/like/game/".$data->getId(), [
            'headers' => [
                'Authorization' => $request->headers->get('Authorization'),
            ],
        ]);
        $likes = json_decode($response->getContent());
        $data->setLikes(count($likes));

        if(!is_null($this->getUser())) {
            $data->setHasLiked(false);
            foreach ($likes as $like) {
                if ($like->user_id == $this->getUser()->getId()) {
                    $data->setHasLiked(true);
                }
            }

            $response = $client->request('GET', "http://wishes-api:3000/wish/game/".$data->getId(), [
                'headers' => [
                    'Authorization' => $request->headers->get('Authorization'),
                ],
            ]);
            $wishes = json_decode($response->getContent());
            $data->setWished(false);
            foreach ($wishes as $wish) {
                if ($wish->user_id == $this->getUser()->getId()) {
                    $data->setWished(true);
                }
            }
        }

        return $data;
    }
}