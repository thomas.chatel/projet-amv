<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Studio;
use App\Repository\GameRepository;
use App\Service\GamesApiService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Flex\Response;

class SearchGamesController extends AbstractController {

    public function __invoke($data, Request $request, GamesApiService $gamesApiService,
                                GameRepository $gameRepository, ManagerRegistry $doctrine) {
        if($data->getTotalItems() < 1){
            $results = $gamesApiService->searchGames($request->query->get("search"));
            $results = json_decode($results);
            $results = $results->results;
            $games = [];
            foreach ($results as $result) {
                $entityManager = $doctrine->getManager();
                $existingGames = $gameRepository->findBy(['name' => $result->name]);
                if (empty($existingGames)) {
                    $details = $gamesApiService->getDetails($result->id);
                    $details = json_decode($details);

                    $game = new Game();
                    $game->setName($details->name);
                    $game->setImage($details->background_image);

                    if (!empty($details->publishers)) {
                        $publisher = $details->publishers[0];

                        $studioRepository = $doctrine->getRepository(Studio::class);

                        $existingStudios = $studioRepository->findBy(['name' => $publisher->name]);

                        if (empty($existingStudios)) {
                            $studio = new Studio();
                            $studio->setName($publisher->name);
                            $studio->setImage($publisher->image_background);
                            $game->setStudio($studio);
                            $entityManager->persist($studio);
                        } else {
                            $game->setStudio($existingStudios[0]);
                        }
                    }
                    $entityManager->persist($game);
                    $entityManager->flush();
                    array_push($games, $game);
                } else {
                    $games = array_merge($games, $existingGames);
                }
            }
            return $games;
        }
        return $data;
    }
}