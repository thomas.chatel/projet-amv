<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RemoveAdminController extends AbstractController
{

    public function __invoke(User $data, ManagerRegistry $doctrine) {
        $entityManager = $doctrine->getManager();

        $roles = $data->getRoles();
        unset($roles[array_search('ROLE_ADMIN', $roles)]);
        $data->setRoles($roles);
        $entityManager->persist($data);

        return $data;
    }
}