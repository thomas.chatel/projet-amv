<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class GamesApiService {

    private $client;
    //private $baseUrl;
    //private $apikey;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
        //$this->baseUrl = getenv('RAWG_API_URL');
        //$this->apikey = getenv('RAWG_API_APIKEY');
    }

    public function getGames() {
        $response = $this->client->request(
            'GET',
            "https://api.rawg.io/api/games?key=039d62c453c340edbd411d63f04ed2d4"//"$this->baseUrl/api/games?key=$this->apikey"
        );

        return $response->getContent();
    }

    public function searchGames($search) {
        $response = $this->client->request(
            'GET',
            "https://api.rawg.io/api/games?search=$search&key=039d62c453c340edbd411d63f04ed2d4"//"$this->baseUrl/api/games?key=$this->apikey"
        );
        return $response->getContent();
    }

    public function getDetails($id) {
        $response = $this->client->request(
            'GET',
            "https://api.rawg.io/api/games/$id?key=039d62c453c340edbd411d63f04ed2d4"//"$this->baseUrl/api/games/$id?key=$this->apikey"
        );

        return $response->getContent();
    }
}