<?php

namespace App\Command;

use App\Entity\Game;
use App\Entity\Studio;
use App\Controller\GameController;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

use App\Service\GamesApiService;

class ImportGamesCommand extends Command
{

    private $gamesApiService;
    private $doctrine;

    protected static $defaultName = 'import:games';

    public function __construct(ManagerRegistry $doctrine, GamesApiService $gamesApiService) {
        parent::__construct();
        $this->doctrine = $doctrine;
        $this->gamesApiService = $gamesApiService;
    }

    protected function configure(): void {
        $this
            ->setDescription("This command import games from an external API")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {

        $importedGamesCount = 0;
        $importedStudiosCount = 0;

        $games = $this->gamesApiService->getGames();
        $games = json_decode($games);

        $entityManager = $this->doctrine->getManager();
        $gameRepository = $this->doctrine->getRepository(Game::class);

        foreach ($games->results as $result) {

            //check if $result->name exist dans la db
            $existingGames = $gameRepository->findBy(['name' => $result->name]);

            if (empty($existingGames)) {

                $details = $this->gamesApiService->getDetails($result->id);
                $details = json_decode($details);

                $game = new Game();
                $game->setName($details->name);
                $game->setImage($details->background_image);

                if (!empty($details->publishers)) {
                    $publisher = $details->publishers[0];
                    $studioRepository = $this->doctrine->getRepository(Studio::class);

                    $existingStudios = $studioRepository->findBy(['name' => $publisher->name]);

                    if (empty($existingStudios)) {
                        $studio = new Studio();
                        $studio->setName($publisher->name);
                        $game->setStudio($studio);
                        $entityManager->persist($studio);
                        $importedStudiosCount++;
                    } else {
                        $game->setStudio($existingStudios[0]);
                    }
                }
                $entityManager->persist($game);
                $entityManager->flush();
                $importedGamesCount++;
            }
        }

        $io = new SymfonyStyle($input, $output);
        $io->success("$importedStudiosCount studios imported");
        $io->success("$importedGamesCount games imported");

        return Command::SUCCESS;
    }
}