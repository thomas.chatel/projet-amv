<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait TimestampableTrait {

    /**
     * @ORM\Column(type="datetime", options={"default": "NOW()"})
     * @Gedmo\Timestampable(on="create")
     * @Groups({"user_read", "game_read", "studio_read"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", options={"default": "NOW()"})
     * @Gedmo\Timestampable(on="update")
     * @Groups({"user_read", "game_read", "studio_read"})
     */
    private $updatedAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return self
     */
    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return self
     */
    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}