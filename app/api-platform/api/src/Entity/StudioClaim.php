<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\StudioClaimRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\AcceptClaimStudioController;



/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"studio_claim_write"}},
 *     itemOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"studio_claim_read"}},
 *          },
 *         "DELETE"={"security"="is_granted('ROLE_ADMIN') or object.getClaimer() == user"},
 *         "ACCEPT_STUDIO_CLAIM_GET"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "method"="GET",
 *              "path"="/studio_claims/{id}/accept",
 *              "controller"=AcceptClaimStudioController::class
 *         }
 *     },
 *     collectionOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"studios_claims_read"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *         "POST"
 *     }
 * )
 * @ORM\Entity(repositoryClass=StudioClaimRepository::class)
 */
class StudioClaim
{

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"studios_claims_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="studioClaims")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"studio_claim_write", "studios_claims_read"})
     */
    private $claimer;

    /**
     * @ORM\ManyToOne(targetEntity=Studio::class, inversedBy="claims")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"studio_claim_write", "studios_claims_read"})
     */
    private $studio;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClaimer(): ?User
    {
        return $this->claimer;
    }

    public function setClaimer(?User $claimer): self
    {
        $this->claimer = $claimer;

        return $this;
    }

    public function getStudio(): ?Studio
    {
        return $this->studio;
    }

    public function setStudio(?Studio $studio): self
    {
        $this->studio = $studio;

        return $this;
    }
}
