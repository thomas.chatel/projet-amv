<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use App\Repository\StudioRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=StudioRepository::class)
 * @ApiResource(
 *     denormalizationContext={"groups"={"studio_write"}},
 *     itemOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"studio_read"}}
 *          },
 *         "PATCH"={"security"="is_granted('ROLE_ADMIN')"},
 *         "DELETE"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     collectionOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"studios_read"}}
 *          },
 *         "POST"
 *     }
 * )
 */
class Studio
{

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"studio_read", "studios_read", "user_read", "game_read", "games_read", "games_claims_read"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     * )
     * @ORM\Column(type="string", length=255)
     * @Groups({"studio_read", "studios_read", "studio_write", "user_read", "game_read", "games_read", "studios_claims_read", "games_claims_read"})
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="studios")
     * @ORM\JoinColumn(nullable=true)
     * @Groups({"studio_read", "studios_read", "studio_write", "studios_claims_read"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", nullable=true, length=2083)
     * @Groups({"studio_read", "studio_write", "user_read"})
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     * @Groups({"studio_read", "studio_write", "user_read", "studios_claims_read"})
     */
    private $approved;

    /**
     * @ORM\OneToMany(targetEntity=Game::class, mappedBy="studio")
     * @Groups({"studio_read", "studios_read"})
     */
    private $games;

    /**
     * @ORM\OneToMany(targetEntity=GameClaim::class, mappedBy="claimer", orphanRemoval=true)
     */
    private $gameClaims;

    /**
     * @ORM\OneToMany(targetEntity=StudioClaim::class, mappedBy="studio")
     */
    private $claims;

    public function __construct()
    {
        $this->games = new ArrayCollection();
        $this->gameClaims = new ArrayCollection();
        $this->claims = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getApproved(): ?bool
    {
        return $this->approved;
    }

    public function setApproved(bool $approved): self
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGames(): Collection
    {
        return $this->games;
    }

    public function addGame(Game $game): self
    {
        if (!$this->games->contains($game)) {
            $this->games[] = $game;
            $game->setStudio($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->games->contains($game)) {
            $this->games->removeElement($game);
            // set the owning side to null (unless already changed)
            if ($game->getStudio() === $this) {
                $game->setStudio(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GameClaim[]
     */
    public function getGameClaims(): Collection
    {
        return $this->gameClaims;
    }

    public function addGameClaim(GameClaim $gameClaim): self
    {
        if (!$this->gameClaims->contains($gameClaim)) {
            $this->gameClaims[] = $gameClaim;
            $gameClaim->setClaimer($this);
        }

        return $this;
    }

    public function removeGameClaim(GameClaim $gameClaim): self
    {
        if ($this->gameClaims->contains($gameClaim)) {
            $this->gameClaims->removeElement($gameClaim);
            // set the owning side to null (unless already changed)
            if ($gameClaim->getClaimer() === $this) {
                $gameClaim->setClaimer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StudioClaim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(StudioClaim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setStudio($this);
        }

        return $this;
    }

    public function removeClaim(StudioClaim $claim): self
    {
        if ($this->claims->contains($claim)) {
            $this->claims->removeElement($claim);
            // set the owning side to null (unless already changed)
            if ($claim->getStudio() === $this) {
                $claim->setStudio(null);
            }
        }

        return $this;
    }
}
