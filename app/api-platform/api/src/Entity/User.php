<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\AddAdminController;
use App\Controller\RemoveAdminController;
use App\Controller\TokenInfosController;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ApiResource(
 *     denormalizationContext={"groups"={"user_write"}},
 *     itemOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"user_read"}},
 *              "security"="is_granted('ROLE_ADMIN') or object == user"
 *          },
 *         "PATCH"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *         "DELETE"={"security"="is_granted('ROLE_ADMIN') or object == user"},
 *         "ADD_ADMIN_PATCH"={
 *              "method"="PATCH",
 *              "path"="/admin/{id}/add",
 *              "controller"=AddAdminController::class,
 *              "security"="is_granted('ROLE_SUPER_ADMIN')"
 *          },
 *          "REMOVE_ADMIN_DELETE"={
 *              "method"="PATCH",
 *              "path"="/admin/{id}/remove",
 *              "controller"=RemoveAdminController::class,
 *              "security"="is_granted('ROLE_SUPER_ADMIN')"
 *          }
 *     },
 *     collectionOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"users_read"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *         "POST"={
 *              "normalization_context"={"groups"={"users_read"}}
 *          },
 *          "TOKEN_INFOS_GET"={
 *              "method"="GET",
 *              "path"="/token",
 *              "controller"=TokenInfosController::class,
 *          }
 *     }
 * )
 */
class User implements UserInterface
{

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"user_read", "users_read", "studio_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user_read", "users_read", "user_write", "studio_read"})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     min = 5,
     *     max = 180,
     * )
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     * @Groups({"user_read", "users_read"})
     */
    private $roles = [];

    /**
     * @Groups({"user_write"})
     */
    private $plainPassword;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user_read", "users_read", "user_write", "studio_read", "studios_claims_read"})
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     min = 3,
     *     max = 50,
     * )
     */
    private $username;

    /**
     * @ORM\OneToMany(targetEntity=Studio::class, mappedBy="user")
     * @Groups({"user_read"})
     */
    private $studios;

    /**
     * @ORM\OneToMany(targetEntity=StudioClaim::class, mappedBy="claimer", orphanRemoval=true)
     */
    private $studioClaims;

    public function __construct()
    {
        $this->studios = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->wishes = new ArrayCollection();
        $this->studioClaims = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getPlainPassword(): string
    {
        return (string) $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return Collection|Studio[]
     */
    public function getStudios(): Collection
    {
        return $this->studios;
    }

    public function addStudio(Studio $studio): self
    {
        if (!$this->studios->contains($studio)) {
            $this->studios[] = $studio;
            $studio->setUserId($this);
        }

        return $this;
    }

    public function removeStudio(Studio $studio): self
    {
        if ($this->studios->contains($studio)) {
            $this->studios->removeElement($studio);
            // set the owning side to null (unless already changed)
            if ($studio->getUserId() === $this) {
                $studio->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StudioClaim[]
     */
    public function getStudioClaims(): Collection
    {
        return $this->studioClaims;
    }

    public function addStudioClaim(StudioClaim $studioClaim): self
    {
        if (!$this->studioClaims->contains($studioClaim)) {
            $this->studioClaims[] = $studioClaim;
            $studioClaim->setClaimer($this);
        }

        return $this;
    }

    public function removeStudioClaim(StudioClaim $studioClaim): self
    {
        if ($this->studioClaims->contains($studioClaim)) {
            $this->studioClaims->removeElement($studioClaim);
            // set the owning side to null (unless already changed)
            if ($studioClaim->getClaimer() === $this) {
                $studioClaim->setClaimer(null);
            }
        }

        return $this;
    }
}
