<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\TimestampableTrait;
use App\Repository\GameClaimRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\AcceptClaimGameController;


/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"game_claim_write"}},
 *     itemOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"game_claim_read"}},
 *          },
 *         "DELETE"={"security"="is_granted('ROLE_ADMIN') or object.getClaimer().getUser() == user"},
 *         "ACCEPT_GAME_CLAIM_GET"={
 *              "security"="is_granted('ROLE_ADMIN')",
 *              "method"="GET",
 *              "path"="/game_claims/{id}/accept",
 *              "controller"=AcceptClaimGameController::class
 *         }
 *     },
 *     collectionOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"games_claims_read"}},
 *              "security"="is_granted('ROLE_ADMIN')"
 *          },
 *         "POST"={
 *              "security"="is_granted('ROLE_STUDIO')"
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=GameClaimRepository::class)
 */
class GameClaim
{

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"games_claims_read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Studio::class, inversedBy="gameClaims")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"game_claim_write", "games_claims_read"})
     */
    private $claimer;

    /**
     * @ORM\ManyToOne(targetEntity=Game::class, inversedBy="claims")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"game_claim_write", "games_claims_read"})
     */
    private $game;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClaimer(): ?Studio
    {
        return $this->claimer;
    }

    public function setClaimer(?Studio $claimer): self
    {
        $this->claimer = $claimer;

        return $this;
    }

    public function getGame(): ?Game
    {
        return $this->game;
    }

    public function setGame(?Game $game): self
    {
        $this->game = $game;

        return $this;
    }
}
