<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\TimestampableTrait;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Filter\SearchGamesFilter;
use Doctrine\ORM\Mapping as ORM;
use App\Controller\SearchGamesController;
use App\Controller\GetGameController;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"game_write"}},
 *     itemOperations={
 *         "GET"={
 *              "normalization_context"={"groups"={"game_read"}},
 *              "controller"=GetGameController::class
 *          },
 *         "PATCH"={"security"="is_granted('ROLE_ADMIN') or object.getStudio().getUser() == user"},
 *         "DELETE"={"security"="is_granted('ROLE_ADMIN') or object.getStudio().getUser() == user"}
 *     },
 *     collectionOperations={
 *          "GAMES_SEARCH_GET"={
 *              "method"="GET",
 *              "path"="/games/search",
 *              "controller"=SearchGamesController::class,
 *              "filters"={SearchGamesFilter::class},
 *              "defaults"={
 *                   "_api_resource_class"=Game::class,
 *                   "_api_collection_operation_name"="GET"
 *              }
 *          },
 *         "GET"={
 *              "normalization_context"={"groups"={"games_read"}}
 *          },
 *         "POST"={
 *              "security"="is_granted('ROLE_STUDIO') or object.getStudio().getUser() == user",
 *              "normalization_context"={"groups"={"game_write"}}
 *          }
 *     }
 * )
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{

    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"games_read", "game_read", "studio_read", "games_claims_read"})
     */
    private $id;

    /**
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *     min = 1,
     *     max = 255,
     * )
     * @ORM\Column(type="string", length=255)
     * @Groups({"game_read", "games_read", "game_write", "studio_read", "games_claims_read"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true, length=2083)
     * @Groups({"game_read", "games_read", "game_write", "studio_read"})
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity=UrlRedirect::class, mappedBy="game", cascade={"persist", "remove", "merge"})
     * @Groups({"game_read", "game_write"})
     */
    private $urlRedirects;

    /**
     * @ORM\ManyToOne(targetEntity=Studio::class, inversedBy="games")
     * @Groups({"game_read", "games_read", "game_write"})
     */
    private $studio;

    /**
     * @Groups({"game_read"})
     */
    private $likes;

    /**
     * @Groups({"game_read"})
     */
    private $hasLiked;

    /**
     * @Groups({"game_read"})
     */
    private $wished;

    /**
     * @ORM\OneToMany(targetEntity=GameClaim::class, mappedBy="game")
     */
    private $claims;

    public function __construct()
    {
        $this->urlRedirects = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->claims = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|UrlRedirect[]
     */
    public function getUrlRedirects(): Collection
    {
        return $this->urlRedirects;
    }

    public function addUrlRedirect(UrlRedirect $urlRedirect): self
    {
        if (!$this->urlRedirects->contains($urlRedirect)) {
            $this->urlRedirects[] = $urlRedirect;
            $urlRedirect->setGame($this);
        }

        return $this;
    }

    public function removeUrlRedirect(UrlRedirect $urlRedirect): self
    {
        if ($this->urlRedirects->contains($urlRedirect)) {
            $this->urlRedirects->removeElement($urlRedirect);
            // set the owning side to null (unless already changed)
            if ($urlRedirect->getGame() === $this) {
                $urlRedirect->setGame(null);
            }
        }

        return $this;
    }

    public function getStudio(): ?Studio
    {
        return $this->studio;
    }

    public function setStudio(?Studio $studio): self
    {
        $this->studio = $studio;

        return $this;
    }

    public function getLikes()
    {
        return $this->likes;
    }

    public function setLikes(int $likes): self
    {
        $this->likes = $likes;

        return $this;
    }

    public function getHasLiked()
    {
        return $this->hasLiked;
    }

    public function setHasLiked(bool $hasLiked): self
    {
        $this->hasLiked = $hasLiked;

        return $this;
    }

    public function getWished()
    {
        return $this->wished;
    }

    public function setWished(bool $wished): self
    {
        $this->wished = $wished;

        return $this;
    }

    /**
     * @return Collection|GameClaim[]
     */
    public function getClaims(): Collection
    {
        return $this->claims;
    }

    public function addClaim(GameClaim $claim): self
    {
        if (!$this->claims->contains($claim)) {
            $this->claims[] = $claim;
            $claim->setGame($this);
        }

        return $this;
    }

    public function removeClaim(GameClaim $claim): self
    {
        if ($this->claims->contains($claim)) {
            $this->claims->removeElement($claim);
            // set the owning side to null (unless already changed)
            if ($claim->getGame() === $this) {
                $claim->setGame(null);
            }
        }

        return $this;
    }
}
