<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends Fixture {

    public function load(ObjectManager $manager) {

        $super_admin = new User();
        $super_admin
            ->setRoles(["ROLE_SUPER_ADMIN"])
            ->setEmail("test@superadmin.fr")
            ->setPlainPassword("testtest")
            ->setUsername("superadmin")
        ;
        $manager->persist($super_admin);

        $admin = new User();
        $admin
            ->setRoles(["ROLE_ADMIN"])
            ->setEmail("test@admin.fr")
            ->setPlainPassword("testtest")
            ->setUsername("admin")
        ;
        $manager->persist($admin);

        $user = new User();
        $user
            ->setRoles(["ROLE_USER"])
            ->setEmail("test@user.fr")
            ->setPlainPassword("testtest")
            ->setUsername("user")
        ;
        $manager->persist($user);

        $manager->flush();
    }
}