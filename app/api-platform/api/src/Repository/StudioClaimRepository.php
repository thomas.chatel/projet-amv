<?php

namespace App\Repository;

use App\Entity\StudioClaim;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StudioClaim|null find($id, $lockMode = null, $lockVersion = null)
 * @method StudioClaim|null findOneBy(array $criteria, array $orderBy = null)
 * @method StudioClaim[]    findAll()
 * @method StudioClaim[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StudioClaimRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StudioClaim::class);
    }

    // /**
    //  * @return StudioClaim[] Returns an array of StudioClaim objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?StudioClaim
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
