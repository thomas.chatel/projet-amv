<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class SearchGamesFilter extends AbstractFilter
{
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
        if ($property == "search") {
            $alias = $queryBuilder->getRootAlias();
            $queryBuilder
                ->andWhere("LOWER($alias.name) LIKE :value")
                ->setParameter("value", "%". strtolower($value) ."%")
            ;
        }
    }

    public function getDescription(string $resourceClass): array {
        return [];
    }
}