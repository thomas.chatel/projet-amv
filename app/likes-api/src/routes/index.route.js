const express = require("express");
const likeController = require("../controllers/like.controller");
const { haveAdminRole, isAuthenticated } = require("../middlewares/security")

const router = express.Router();

router.get("/", haveAdminRole, likeController.findAllLikes);
router.post("/", isAuthenticated, likeController.createLike);
router.get("/:id", isAuthenticated, likeController.findLikeById);
router.get("/game/:id", likeController.findLikesByGameId);
router.get("/user/:id", isAuthenticated, likeController.findLikesByUserId);
router.patch("/:id", isAuthenticated, likeController.updateLike);
router.delete("/", isAuthenticated, likeController.deleteLike);

module.exports = router;
