const mongoose = require("mongoose");

const LikeSchema = new mongoose.Schema(
  {
    user_id: { type: Number, required: true },
    game_id: { type: Number, required: true },
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("Like", LikeSchema, "like");
