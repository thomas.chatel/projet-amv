const fetch = require("node-fetch");
const LikeService = require("../services/like.service");

const likeController = {
  findAllLikes: async (req, res) => {
    try {
      const likes = await LikeService.find();
      res.status(200).json(likes);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findLikeById: async (req, res) => {
    try {
      const like = await LikeService.findById(req.params.id);
      res.status(200).json(like);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findLikesByGameId: async (req, res) => {
    try {
      const like = await LikeService.findByParams({ game_id: req.params.id });
      res.status(200).json(like);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findLikesByUserId: async (req, res) => {
    try {
      const like = await LikeService.findByParams({ user_id: req.params.id });
      res.status(200).json(like);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  createLike: async (req, res) => {
    try {
      const data = {
        ...req.body,
      };

      const gameRes = await fetch('http://api:80/games/' + data.game_id, {
        headers: {
          "Authorization": req.headers['authorization']
        }
      });
      if (gameRes.status !== 200) {
        return res.status(gameRes.status).send(gameRes.error)
      }

      const userRes = await fetch('http://api:80/users/' + data.user_id, {
        headers: {
          "Authorization": req.headers['authorization']
        }
      });
      if (userRes.status !== 200) {
        return res.status(userRes.status).send(userRes.error)
      }

      if(req.user.id !== data.user_id && !req.user.roles.includes(["ROLE_ADMIN"])
          && !req.user.roles.includes(["ROLE_SUPER_ADMIN"])) {
        return res.sendStatus(403);
      }

      const like = await LikeService.create(data);
      res.status(201).json(like);
    } catch (error) {
      console.error(error);
      res.sendStatus(500);
    }
  },
  updateLike: async (req, res) => {
    try {
      const like = await LikeService.update(req.params.id, req.body);
      res.status(200).json(like);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  deleteLike: async (req, res) => {
    try {
      const { user_id, game_id } = req.body
      const like = await LikeService.findByParams({ user_id: user_id, game_id: game_id});
      await LikeService.delete(like[0]._id);
      res.sendStatus(204);
    } catch (error) {
      res.sendStatus(500);
    }
  },
};

module.exports = likeController;
