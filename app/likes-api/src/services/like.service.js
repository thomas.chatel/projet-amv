const Like = require("../models/like.model");

const likeService = {
  find: async () => {
    return Like.find();
  },
  findByParams: async (query) => {
    const like = await Like.find(query);

    if (like !== null) {
      return like;
    } else {
      throw new Error("Like not found");
    }
  },
  findById: async (id) => {
    try {
      const like = await Like.findById(id).exec();

      if (like !== null) {
        return like;
      } else {
        throw new Error();
      }
    } catch (error) {
      throw new Error("Like not found");
    }
  },
  create: async (data) => {
    const newLike = await Like.create(data);

    return newLike;
  },
  update: async (id, data) => {
    const updatedLike = await Like.findByIdAndUpdate(id, data, {
      new: true,
      runValidators: true,
    });

    if (updatedLike !== null) {
      return updatedLike;
    } else {
      throw new Error("Like not found");
    }
  },
  delete: async (id) => {
    const deletedLike = await Like.findByIdAndDelete(id);

    if (deletedLike !== null) {
      return deletedLike;
    } else {
      throw new Error("Like not found");
    }
  },
};

module.exports = likeService;
