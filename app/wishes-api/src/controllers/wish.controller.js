const WishService = require("../services/wish.service");
const fetch = require("node-fetch")

const wishController = {
  findAllWishes: async (req, res) => {
    try {
      const wishes = await WishService.find();
      res.status(200).json(wishes);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findWishById: async (req, res) => {
    try {
      const wish = await WishService.findById(req.params.id);
      res.status(200).json(wish);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findWishesByGameId: async (req, res) => {
    try {
      const wish = await WishService.findByParams({ game_id: req.params.id });
      res.status(200).json(wish);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findWishesByUserId: async (req, res) => {
    try {
      const wish = await WishService.findByParams({ user_id: req.params.id });
      res.status(200).json(wish);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  createWish: async (req, res) => {
    try {
      const data = {
        ...req.body,
      };

      const gameRes = await fetch('http://api:80/games/' + data.game_id, {
        headers: {
          "Authorization": req.headers['authorization']
        }
      });
      if (gameRes.status !== 200) {
        return res.status(gameRes.status).send(gameRes.error)
      }

      const userRes = await fetch('http://api:80/users/' + data.user_id, {
        headers: {
          "Authorization": req.headers['authorization']
        }
      });
      if (userRes.status !== 200) {
        return res.status(userRes.status).send(userRes.error)
      }

      if(req.user.id !== data.user_id && !req.user.roles.includes(["ROLE_ADMIN"])
          && !req.user.roles.includes(["ROLE_SUPER_ADMIN"])) {
        return res.sendStatus(403);
      }

      const wish = await WishService.create(data);
      res.status(201).json(wish);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  updateWish: async (req, res) => {
    try {
      const wish = await WishService.update(req.params.id, req.body);
      res.status(200).json(wish);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  deleteWish: async (req, res) => {
    try {
      const { user_id, game_id } = req.body
      const wish = await WishService.findByParams({ user_id: user_id, game_id: game_id});
      await WishService.delete(wish[0]._id);
      res.sendStatus(204);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  deleteWishById: async (req, res) => {
    try {
      await WishService.delete(req.params.id);
      res.sendStatus(204);
    } catch (error) {
      res.sendStatus(500);
    }
  },
};

module.exports = wishController;
