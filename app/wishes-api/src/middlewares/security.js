const fetch = require('node-fetch');

const haveAdminRole = async (req, res, next) => {
    const token = req.headers['authorization'];
    if (token === undefined) {
        return res.sendStatus(401);
    }

    const tokenInfos = await getTokenInfos(token);
    const roles = tokenInfos.roles["hydra:member"];
    req.user = {
        id: tokenInfos.id,
        roles: roles
    }

    if (roles.includes("ROLE_ADMIN") || roles.includes("ROLE_SUPER_ADMIN")) {
        next();
    } else {
        return res.sendStatus(403);
    }
}

const isAuthenticated = async (req, res, next) => {
    const token = req.headers['authorization'];
    if (token === undefined) {
        return res.sendStatus(401);
    }

    const tokenInfos = await getTokenInfos(token);

    req.user = {
        id: tokenInfos.id,
        roles: tokenInfos.roles["hydra:member"]
    }

    if (tokenInfos.id) {
        next();
    } else {
        return res.sendStatus(403);
    }
}

const getTokenInfos = async (token) => {
    const res = await fetch("http://api:80/token", {
        headers: {
            "Authorization": token
        }
    });
    return await res.json();
}

module.exports = {
    haveAdminRole: haveAdminRole,
    isAuthenticated: isAuthenticated
}