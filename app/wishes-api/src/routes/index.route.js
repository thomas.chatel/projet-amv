const express = require("express");
const wishController = require("../controllers/wish.controller");
const { haveAdminRole, isAuthenticated } = require("../middlewares/security");

const router = express.Router();

router.get("/", haveAdminRole, wishController.findAllWishes);
router.post("/", isAuthenticated, wishController.createWish);
router.get("/:id", isAuthenticated, wishController.findWishById);
router.get("/game/:id", wishController.findWishesByGameId);
router.get("/user/:id", isAuthenticated, wishController.findWishesByUserId);
router.delete("/:id", isAuthenticated,wishController.deleteWishById);
router.delete("/", isAuthenticated,wishController.deleteWish);

module.exports = router;
