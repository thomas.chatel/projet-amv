const Wish = require("../models/wish.model");

const wishService = {
  find: async () => {
    return Wish.find();
  },
  findByParams: async (query) => {
    const wish = await Wish.find(query);

    if (wish !== null) {
      return wish;
    } else {
      throw new Error("Wish not found");
    }
  },
  findById: async (id) => {
    try {
      const wish = await Wish.findById(id).exec();

      if (wish !== null) {
        return wish;
      } else {
        throw new Error();
      }
    } catch (error) {
      throw new Error("Wish not found");
    }
  },
  create: async (data) => {
    const newWish = await Wish.create(data);

    return newWish;
  },
  update: async (id, data) => {
    const updatedWish = await Wish.findByIdAndUpdate(id, data, {
      new: true,
      runValidators: true,
    });

    if (updatedWish !== null) {
      return updatedWish;
    } else {
      throw new Error("Wish not found");
    }
  },
  delete: async (id) => {
    const deletedWish = await Wish.findByIdAndDelete(id);

    if (deletedWish !== null) {
      return deletedWish;
    } else {
      throw new Error("Wish not found");
    }
  },
};

module.exports = wishService;
