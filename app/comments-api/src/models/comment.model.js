const mongoose = require("mongoose");

const CommentSchema = new mongoose.Schema(
  {
    content: { type: String, required: true },
    date: { type: Date, required: true, default: Date.now },
    user_id: { type: Number, required: true },
    game_id: { type: Number, required: true },
  },
  {
    versionKey: false,
  }
);

module.exports = mongoose.model("Comment", CommentSchema, "comment");
