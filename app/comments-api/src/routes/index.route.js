const express = require("express");
const commentController = require("../controllers/comment.controller");
const { haveAdminRole, isAuthenticated } = require("../middlewares/security")

const router = express.Router();

router.post("/", isAuthenticated,commentController.createComment);
router.get("/", haveAdminRole,commentController.findAllComments);
router.get("/game/:id", commentController.findCommentsByGameId);
router.delete("/:id", haveAdminRole, commentController.deleteComment);

module.exports = router;
