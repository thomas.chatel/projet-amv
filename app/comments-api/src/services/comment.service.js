const Comment = require("../models/comment.model");
const mongoose = require("mongoose");

const commentService = {
  find: async () => {
    return Comment.find();
  },
  findByParams: async (query) => {
    const comment = await Comment.find(query);

    if (comment !== null) {
      return comment;
    } else {
      throw new Error("Comment not found");
    }
  },
  findById: async (id) => {
    try {
      const comment = await Comment.findById(id);
      console.log("comment="+comment)
      if (comment !== null) {
        return comment;
      } else {
        throw new Error();
      }
    } catch (error) {
      console.log("error="+error)
      throw new Error("Comment not found");
    }
  },
  create: async (data) => {
    const newComment = await Comment.create(data);

    return newComment;
  },
  update: async (id, data) => {
    const updatedComment = await Comment.findByIdAndUpdate(id, data, {
      new: true,
      runValidators: true,
    });

    if (updatedComment !== null) {
      return updatedComment;
    } else {
      throw new Error("Comment not found");
    }
  },
  delete: async (id) => {
    const deletedComment = await Comment.findByIdAndDelete(id);

    if (deletedComment !== null) {
      return deletedComment;
    } else {
      throw new Error("Comment not found");
    }
  },
};

module.exports = commentService;
