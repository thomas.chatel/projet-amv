const indexRouter = require("./routes/index.route");
const databaseConfig = require("./config/database.config");
const dotenv = require("dotenv");
const express = require("express");
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(express.urlencoded());
app.use(cors());

dotenv.config();

databaseConfig.MongoDB().catch((err) => console.log(err));

app.use("/comment", indexRouter);

app.listen(process.env.PORT || 3000, () => console.log("server listening"));
