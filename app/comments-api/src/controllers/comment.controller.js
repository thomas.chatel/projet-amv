const CommentService = require("../services/comment.service");
const fetch = require('node-fetch');

const commentController = {
  findAllComments: async (req, res) => {
    try {
      const comments = await CommentService.find();
      res.status(200).json(comments);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findCommentById: async (req, res) => {
    try {
      const comment = await CommentService.findById(req.params.id);
      res.status(200).json(comment);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  findCommentsByGameId: async (req, res) => {
    try {
      const comments = await CommentService.findByParams({ game_id: req.params.id });
      res.status(200).json(comments);
    } catch (error) {
      console.error(error)
      res.sendStatus(500);
    }
  },
  findCommentsByUserId: async (req, res) => {
    try {
      const comment = await CommentService.findByParams({ user_id: req.params.id });
      res.status(200).json(comment);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  createComment: async (req, res) => {
    try {
      const data = {
        ...req.body,
      };

      const gameRes = await fetch('http://api:80/games/' + data.game_id, {
        headers: {
          "Authorization": req.headers['authorization']
        }
      });
      if (gameRes.status !== 200) {
        return res.status(gameRes.status).send(gameRes.error)
      }

      const userRes = await fetch('http://api:80/users/' + data.user_id, {
        headers: {
          "Authorization": req.headers['authorization']
        }
      });
      if (userRes.status !== 200) {
        return res.status(userRes.status).send(userRes.error)
      }

      if(req.user.id !== data.user_id && !req.user.roles.includes(["ROLE_ADMIN"])
          && !req.user.roles.includes(["ROLE_SUPER_ADMIN"])) {
        return res.sendStatus(403);
      }

      const comment = await CommentService.create(data);
      res.status(201).json(comment);
    } catch (error) {
      console.error(error);
      res.sendStatus(500);
    }
  },
  updateComment: async (req, res) => {
    try {
      const comment = await CommentService.update(req.params.id, req.body);
      res.status(200).json(comment);
    } catch (error) {
      res.sendStatus(500);
    }
  },
  deleteComment: async (req, res) => {
    try {
      await CommentService.delete(req.params.id);
      res.sendStatus(204);
    } catch (error) {
      res.sendStatus(500);
    }
  },
};

module.exports = commentController;
