# Game box

## Installation

- Cloner le répertoire git

```
git clone https://gitlab.com/thomas.chatel/projet-amv.git

```

- Lancement du projet

```
docker-compose build --no-cache
docker-compose up -d
```

- Génération JWT

```
docker-compose exec php sh -c '
    set -e
    apk add openssl
    php bin/console lexik:jwt:generate-keypair
    setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
    setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
'
```

- Aller sur l'url http://localhost:3000
